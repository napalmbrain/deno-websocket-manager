import { Observable } from "./observable.ts";
import { Signaler } from "./signaler.ts";
import { Timeout } from "./timeout.ts";

export const TIMEOUT = 10_000;
export const KEEPALIVE = TIMEOUT / 2;

export function keepalive(keepalive: number) {
  return (
    keepalive - keepalive / 2 + Math.floor((keepalive / 2) * Math.random())
  );
}

export function sleep(milliseconds: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, milliseconds);
  });
}

export type Generic = Record<string | number, unknown>;

export type Message = Generic;

export type Handler = (
  connection: Connection,
  message: Message,
) => void | Promise<void>;

export enum PROTOCOL {
  init = "init",
  init_ack = "init_ack",
  ping = "ping",
  pong = "pong",
  meta = "meta",
  meta_ack = "meta_ack",
}

export interface Action<T> {
  message: T;
  sent: boolean;
  observable: Observable<T>;
  timeout: Timeout;
  closed: Signaler<void>;
}

export interface Connection {
  socket: WebSocket;
  url?: URL;
  id?: string;
  remote?: string;
  actions?: Map<string, Action<Message>>;
  state?: Signaler<State>;
  keepalive?: Timeout;
  timeout?: Timeout;
  meta?: Generic;
}

export enum STATE {
  connected,
  disconnected,
}

export interface State {
  status: STATE;
}

export interface Deferred<T> {
  promise: Promise<T>;
  resolve: (value: T) => void;
}
