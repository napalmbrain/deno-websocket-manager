/**
 * Determines if the environment is the browser.
 * @returns true if the environment is the browser, else false.
 */
export function isBrowser() {
  if (!("Deno" in window)) {
    return true;
  }
  return false;
}
