import { Deferred } from "./common.ts";

export class Signaler<T> {
  public value?: T;
  private deferreds: Deferred<T>[];

  constructor() {
    this.deferreds = [];
  }

  public promise(): Promise<T> {
    const deferred = {} as Deferred<T>;
    deferred.promise = new Promise<T>((resolve) => {
      deferred.resolve = resolve;
    });
    if (this.value) {
      deferred.resolve(this.value);
    } else {
      this.deferreds.push(deferred);
    }
    return deferred.promise;
  }

  public next(): Promise<T> {
    const deferred = {} as Deferred<T>;
    deferred.promise = new Promise<T>((resolve) => {
      deferred.resolve = resolve;
    });
    this.deferreds.push(deferred);
    return deferred.promise;
  }

  public unpromise(promise: Promise<T>) {
    const deferred = this.deferreds.find(({ promise: p }) => promise === p);
    if (deferred) {
      this.deferreds = this.deferreds.filter((d) => deferred !== d);
    }
  }

  public reset() {
    this.value = undefined;
  }

  public signal(value: T) {
    this.value = value;
    for (const deferred of this.deferreds) {
      deferred.resolve(this.value);
    }
    this.deferreds = [];
  }
}
