//import { isBrowser } from "./utils.ts";

export class Timeout {
  private callback: () => void;
  //@ts-ignore: handle is set in the constructor via `start`..
  private handle: number;
  private callbacks: (() => void)[];

  public timeout: number;

  constructor(callback: () => void, timeout: number) {
    this.callback = callback;
    this.callbacks = [];
    this.timeout = timeout;
    this.start();
  }

  public start() {
    this.handle = setTimeout(() => {
      this.callback();
      for (const callback of this.callbacks) {
        callback();
      }
    }, this.timeout);
    /*
    if (!isBrowser()) {
      Deno.unrefTimer(this.handle);
    }
    */
  }

  public stop() {
    clearTimeout(this.handle);
  }

  public restart() {
    this.stop();
    this.start();
  }

  public on(callback: () => void) {
    this.callbacks.push(callback);
  }

  public off(callback: () => void) {
    this.callbacks = this.callbacks.filter((c) => callback !== c);
  }
}
