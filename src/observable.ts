export interface Observer<T> {
  next?: (data: T) => Promise<void> | void;
  error?: (data: T) => Promise<void> | void;
  complete?: () => Promise<void> | void;
}

/**
 * Observable class.
 */
export class Observable<T> {
  /**
   * The array of observers.
   */
  observers: Observer<T>[];

  constructor(observers: Observer<T>[] = []) {
    this.observers = observers;
  }

  /**
   * Call the next function on all observers, if it exists.
   * @param data Data to pass to observers.
   */
  public next(data: T) {
    for (const observer of this.observers) {
      observer.next?.(data);
    }
  }

  /**
   * Call the error function on all observers, if it exists.
   * @param data Data to pass to observers.
   */
  public error(data: T) {
    for (const observer of this.observers) {
      observer.error?.(data);
    }
  }

  /**
   * Call the complete function on all observers, if it exists.
   */
  public complete() {
    for (const observer of this.observers) {
      observer.complete?.();
    }
    this.observers = this.observers.filter(() => false);
  }

  /**
   * Add an observer to the array of observers.
   * @param observer The observer to add.
   */
  public observe(observer: Observer<T>) {
    this.observers.push(observer);
  }

  /**
   * Remove an observer from the array of observers.
   * @param observer The observer to remove.
   */
  public unobserve(observer: Observer<T>) {
    this.observers = this.observers.filter((o) => observer !== o);
  }
}
