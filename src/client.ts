import { Connection, PROTOCOL, STATE } from "./common.ts";
import { Manager } from "./manager.ts";

export class Client extends Manager {
  public connect(url: URL): Connection {
    console.log("CONNECT");
    const socket = new WebSocket(url);
    const connection: Connection = {
      url,
      socket,
    };
    this.attach(connection);
    connection.timeout!.on(() => {
      this.reconnect(connection);
    });
    return connection;
  }

  protected async reconnect(connection: Connection) {
    console.log("RECONNECT");
    const actions = connection.actions;
    const meta = connection.meta;
    connection.socket = new WebSocket(connection.url!);
    this.attach(connection);
    for (const [id, action] of actions!) {
      if (
        (action.message.type === PROTOCOL.init) ||
        (action.message.type === PROTOCOL.init_ack)
      ) {
        continue;
      }
      action.timeout!.stop();
      connection.actions?.set(id, action);
    }
    connection.timeout!.on(() => {
      this.reconnect(connection);
    });
    console.log("AWAITING STATE");
    const { status } = await connection.state!.promise();
    console.log("STATE STATUS", status);
    if (status === STATE.connected) {
      this.meta(connection, meta!);
      for (const action of connection.actions!.values()) {
        this.dispatch(connection, action);
      }
    }
  }

  protected onclose(connection: Connection) {
    //super.onclose(connection);
    console.log("CLOSE");
    this.reconnect(connection);
  }
}
