import {
  Action,
  Connection,
  Generic,
  Handler,
  KEEPALIVE,
  keepalive,
  Message,
  PROTOCOL,
  STATE,
  State,
  TIMEOUT,
} from "./common.ts";
import { Signaler } from "./signaler.ts";
import { Timeout } from "./timeout.ts";
import { Observable, Observer } from "./observable.ts";

export interface ManagerOptions {
  keepalive?: boolean;
}

export class Manager {
  protected options: ManagerOptions;

  protected connections: Map<string, Connection>;
  private handlers: Map<string, Handler>;

  public id: string;

  constructor(options: ManagerOptions = {}) {
    this.options = options;
    this.connections = new Map<string, Connection>();
    this.handlers = new Map<string, Handler>();
    this.id = crypto.randomUUID();
    this.handle(PROTOCOL.init, (connection, message) => {
      console.log("INIT");
      this.send(connection, {
        id: message.id,
        remote: this.id,
        type: PROTOCOL.init_ack,
      });
    });
    this.handle(PROTOCOL.init_ack, (connection, message) => {
      console.log("ACK");
      connection.remote = message.remote as string;
      connection.actions?.get(message.id as string)?.observable.complete();
    });
    this.handle(PROTOCOL.ping, (connection, message) => {
      console.log("PING");
      connection.timeout?.restart();
      this.send(connection, { id: message.id, type: PROTOCOL.pong });
    });
    this.handle(PROTOCOL.pong, (connection, message) => {
      console.log("PONG");
      connection.timeout?.restart();
      connection.actions?.get(message.id as string)?.observable.complete();
    });
    this.handle(PROTOCOL.meta, (connection, message) => {
      Object.assign(connection.meta!, message.meta);
      this.send(connection, {
        id: message.id,
        type: PROTOCOL.meta_ack,
        meta: connection.meta!,
      });
    });
    this.handle(PROTOCOL.meta_ack, (connection, message) => {
      Object.assign(connection.meta!, message.meta);
      connection.actions?.get(message.id as string)?.observable.next(message);
      connection.actions?.get(message.id as string)?.observable.complete();
    });
  }

  protected init(connection: Connection) {
    connection.actions = new Map<string, Action<Message>>();
    connection.state = new Signaler<State>();
    connection.timeout = new Timeout(() => {
      console.log("TIMEOUT");
      connection.keepalive?.stop();
      connection.timeout!.stop();
      connection.state!.signal({ status: STATE.disconnected });
    }, TIMEOUT);
    if (this.options.keepalive) {
      connection.keepalive = new Timeout(() => {
        this.message(connection, { type: PROTOCOL.ping });
        connection.keepalive!.timeout = keepalive(KEEPALIVE);
        connection.keepalive!.restart();
      }, keepalive(KEEPALIVE));
    }
    connection.meta = {};
  }

  protected deinit(connection: Connection) {
    connection.keepalive?.stop();
    connection.timeout!.stop();
    connection.state!.signal({ status: STATE.disconnected });
  }

  protected bind(connection: Connection) {
    connection.socket.onopen = this.onopen.bind(this, connection);
    connection.socket.onmessage = this.onmessage.bind(this, connection);
    connection.socket.onerror = this.onerror.bind(this, connection);
    connection.state!.promise().then(({ status }) => {
      if (status === STATE.connected) {
        connection.socket.onclose = this.onclose.bind(this, connection);
      }
    });
  }

  protected unbind(connection: Connection) {
    connection.socket.onopen = null;
    connection.socket.onmessage = null;
    connection.socket.onerror = null;
    connection.socket.onclose = null;
  }

  protected attach(connection: Connection) {
    console.log("ATTACH");
    do {
      connection.id = crypto.randomUUID();
    } while (this.connections.has(connection.id));
    this.init(connection);
    this.bind(connection);
    this.connections.set(connection.id, connection);
  }

  protected detach(connection: Connection) {
    console.log("DETACH");
    this.deinit(connection);
    this.unbind(connection);
    this.connections.delete(connection.id!);
  }

  protected handle(type: string, handler: Handler) {
    this.handlers.set(type, handler);
  }

  protected send(connection: Connection, message: Message) {
    connection.socket.send(JSON.stringify(message));
  }

  protected close(connection: Connection, code?: number, reason?: string) {
    connection.socket.close(code, reason);
  }

  protected action(
    connection: Connection,
    message: Message,
    observers: Observer<Message>[] = [],
  ): Action<Message> {
    do {
      message.id = crypto.randomUUID();
    } while (connection.actions?.has(message.id as string));
    const action: Action<Message> = {
      message,
      sent: false,
      observable: new Observable<Message>(observers),
      timeout: new Timeout(() => {
        console.log("TIMEOUT");
        action.observable.complete();
      }, TIMEOUT),
      closed: new Signaler<void>(),
    };
    action.timeout!.stop();
    action.observable.observe({
      complete: () => {
        console.log("COMPLETE");
        action.timeout.stop();
        action.closed.signal();
        connection.actions?.delete(action.message.id as string);
      },
    });
    connection.actions?.set(message.id as string, action);
    return action;
  }

  protected dispatch(connection: Connection, action: Action<Message>) {
    action.timeout!.start();
    action.sent = true;
    this.send(connection, action.message);
  }

  public async message(
    connection: Connection,
    message: Message,
    observers: Observer<Message>[] = [],
  ) {
    const action = this.action(connection, message, observers);
    console.log("AWAITING");
    const { status } = await connection.state!.promise();
    if (status === STATE.connected) {
      console.log("SENDING");
      this.dispatch(connection, action);
    }
  }

  public meta(
    connection: Connection,
    payload: Generic,
    observers: Observer<Generic>[] = [],
  ): Promise<Generic> {
    return new Promise<Generic>((resolve) => {
      let meta: Generic;
      observers.push({
        next: (message) => {
          meta = message.meta as Generic;
        },
        complete: () => {
          resolve(meta);
        },
      });
      this.message(
        connection,
        { type: PROTOCOL.meta, meta: payload },
        observers,
      );
    });
  }

  protected onopen(connection: Connection) {
    const action = this.action(connection, { type: PROTOCOL.init }, [
      {
        complete: () => {
          connection.state!.signal({ status: STATE.connected });
        },
      },
    ]);
    this.dispatch(connection, action);
  }

  protected onmessage(connection: Connection, event: MessageEvent) {
    const message = JSON.parse(event.data);
    this.handlers.get(message.type)?.(connection, message);
  }

  protected onerror(_connection: Connection, event: Event | ErrorEvent) {
    console.log("WebSocket error:", event);
  }

  protected onclose(_connection: Connection) {
    console.log("WebSocket closed.");
  }
}
