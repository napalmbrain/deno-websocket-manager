import { expect } from "https://deno.land/x/expect@v0.4.0/mod.ts";

import { Signaler } from "../src/signaler.ts";
import { STATE, State } from "../src/common.ts";

Deno.test("signaler", async (t) => {
  await t.step("can signal", async () => {
    const signaler = new Signaler<State>();
    setTimeout(() => {
      signaler.signal({ status: STATE.connected });
    }, 100);
    const { status } = await signaler.promise();
    expect(status).toBe(STATE.connected);
  });

  await t.step("can update signal", async () => {
    const signaler = new Signaler<State>();
    signaler.signal({ status: STATE.disconnected });
    signaler.signal({ status: STATE.connected });
    const { status } = await signaler.promise();
    expect(status).toBe(STATE.connected);
  });

  await t.step("can reset", () => {
    const signaler = new Signaler<State>();
    signaler.signal({ status: STATE.disconnected });
    signaler.reset();
    expect(signaler.value).toBeUndefined();
  });
});
