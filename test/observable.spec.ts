import { expect, mock } from "https://deno.land/x/expect@v0.4.0/mod.ts";

import { Observable, Observer } from "../src/observable.ts";

Deno.test("observable", async (t) => {
  await t.step("can be initialized without observers", () => {
    const observable = new Observable<void>();
    expect(observable).toBeDefined();
  });

  await t.step("can be initialized with observers", () => {
    const fn = mock.fn(() => {});
    const observable = new Observable<void>([
      {
        complete: fn,
      },
    ]);
    observable.complete();
    expect(fn).toHaveBeenCalled();
  });

  await t.step("can call next", () => {
    const fn = mock.fn(() => {});
    const observable = new Observable<void>([
      {
        next: fn,
      },
    ]);
    observable.next();
    expect(fn).toHaveBeenCalled();
  });

  await t.step("can call error", () => {
    const fn = mock.fn(() => {});
    const observable = new Observable<void>([
      {
        error: fn,
      },
    ]);
    observable.error();
    expect(fn).toHaveBeenCalled();
  });

  await t.step("can call complete", () => {
    const fn = mock.fn(() => {});
    const observable = new Observable<void>([
      {
        complete: fn,
      },
    ]);
    observable.complete();
    expect(fn).toHaveBeenCalled();
  });

  await t.step("can add observers", () => {
    const alpha = mock.fn(() => {});
    const beta = mock.fn(() => {});
    const observable = new Observable<void>([
      {
        complete: alpha,
      },
    ]);
    observable.observe({
      complete: beta,
    });
    observable.complete();
    expect(alpha).toHaveBeenCalled();
    expect(beta).toHaveBeenCalled();
  });

  await t.step("can remove observers", () => {
    const alpha = mock.fn(() => {});
    const beta = mock.fn(() => {});
    const observable = new Observable<void>([
      {
        complete: alpha,
      },
    ]);
    const observer: Observer<void> = {
      complete: beta,
    };
    observable.observe(observer);
    observable.unobserve(observer);
    observable.complete();
    expect(alpha).toHaveBeenCalled();
    expect(beta).not.toHaveBeenCalled();
  });
});
