import { expect, mock } from "https://deno.land/x/expect@v0.4.0/mod.ts";

import { sleep } from "../src/common.ts";
import { Timeout } from "../src/timeout.ts";

Deno.test("timeout", async (t) => {
  await t.step("can automatically start", async () => {
    const fn = mock.fn(() => {});
    new Timeout(fn, 50);
    await sleep(100);
    expect(fn).toHaveBeenCalled();
  });

  await t.step("can be stopped", async () => {
    const fn = mock.fn(() => {});
    const timeout = new Timeout(fn, 50);
    timeout.stop();
    await sleep(100);
    expect(fn).not.toHaveBeenCalled();
  });

  await t.step("can be started", async () => {
    const fn = mock.fn(() => {});
    const timeout = new Timeout(fn, 50);
    timeout.stop();
    timeout.start();
    await sleep(100);
    expect(fn).toHaveBeenCalled();
  });

  await t.step("can be restarted", async () => {
    const fn = mock.fn(() => {});
    const timeout = new Timeout(fn, 50);
    timeout.restart();
    await sleep(100);
    expect(fn).toHaveBeenCalled();
  });

  await t.step("can attach callbacks with `on`", async () => {
    const fn = mock.fn(() => {});
    const timeout = new Timeout(() => {}, 50);
    timeout.on(fn);
    await sleep(100);
    expect(fn).toHaveBeenCalled();
  });
});
